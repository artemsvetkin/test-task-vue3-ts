import {createStore} from 'vuex'

type typeItem = null | string

export default createStore({
    state: {
        peoples: {},
        locale: {}
    },
    getters: {
        getPeoples: state => state.peoples,
        getLocale: state => state.locale
    },
    mutations: {
        setPeoples(state, value) {
            state.peoples = value
        },
        getLocale(state, value) {
            state.locale = value
        }
    },
    actions: {
        async fetchPeoples(context) {
            const response = await fetch('https://swapi.dev/api/people/')
            const data = await response.json()
            for (let item in data.results) {
                data.results[item].id = Number(item) + 1
            }
            context.commit('setPeoples', data)
        },
        getLocaleStore(context) {
            const storePeoples: typeItem = localStorage.getItem('peoplesFavorite')
            const peoples: any = storePeoples ? JSON.parse(storePeoples) : null
            context.commit('getLocale', peoples)
        },
        setLocaleStore(context, row) {
            console.log(row)
            const storePeoples: typeItem = localStorage.getItem('peoplesFavorite')
            const peoples: any = storePeoples ? JSON.parse(storePeoples) : null
            localStorage.setItem('peoplesFavorite', JSON.stringify({...peoples, [String(row.id)]: row}))
            context.commit('getLocale', peoples)
        },
        deleteLocaleStore(context, row) {
            const storePeoples: typeItem = localStorage.getItem('peoplesFavorite')
            const peoples: any = storePeoples ? JSON.parse(storePeoples) : null
            for (let person in peoples) {
                if(peoples[person].id === row.id) {
                    delete peoples[person]
                }
            }
            localStorage.setItem('peoplesFavorite', JSON.stringify(peoples))
            context.commit('getLocale', peoples)
        }
    },
    modules: {}
})
