import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import MainPage from '@/views/main.vue'
import FavoritesPage from '@/views/favorites.vue'
import PeoplesPage from '@/views/peoples.vue'
import PersonPage from '@/views/person.vue'
import BaseLayout from  '@/layouts/base-layout.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: BaseLayout,
    children: [
      {path: '', name: 'main', component: MainPage},
      {path: 'peoples', name: 'peoples', component: PeoplesPage},
      {path: 'peoples/:id', name: 'people', component: PersonPage},
      {path: 'favorites', name: 'favorites', component: FavoritesPage},
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
